import-module au

$release = "https://www.avira.com/en/free-antivirus-windows"

function global:au_SearchReplace {
    @{
        'tools\chocolateyInstall.ps1' = @{
            "(^[$]url\s*=\s*)('.*')"      = "`$1'$($Latest.URL32)'"
            "(^[$]checksum\s*=\s*)('.*')" = "`$1'$($Latest.Checksum32)'"
        }
     }
}

function global:au_GetLatest {
    $downloadpage = $((((Invoke-WebRequest -Uri $release -UseBasicParsing).Links | Where-Object {$_ -match 'start-download'}).href)[0])
    $url32 = $((((Invoke-WebRequest -Uri "https://avira.com$($downloadpage)" -UseBasicParsing).Links | Where-Object {$_ -match 'exe'}).href))
    
    Write-host "Downloading"
    Invoke-WebRequest -Uri $url32 -OutFile ./avirafreeantivirus.exe

    Write-host "Installation"
    ./avirafreeantivirus.exe /quiet

    $avirafreeantivirus=$(../../tools/Get-InstalledApps.ps1 -ComputerName $env:COMPUTERNAME -NameRegex 'avirafreeantivirus')
    while($avirafreeantivirus.count -eq 0)
    {
        $avirafreeantivirus=$(../../tools/Get-InstalledApps.ps1 -ComputerName $env:COMPUTERNAME -NameRegex 'avirafreeantivirus')
    }
    $version=$avirafreeantivirus.DisplayVersion
	
	$Latest = @{ URL32 = $url32; Version = $version }
    return $Latest
}

update
Test-Package
