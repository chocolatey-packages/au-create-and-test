﻿$packageName = 'avirafreeantivirus'
$installerType = 'EXE'
$url = 'https://package.avira.com/download/spotlight-windows-bootstrapper/avira_en_sptl1___pavwws-spotlight-release.exe'
$silentArgs = ''
$validExitCodes = @(0)
$checksum = 'd31872b852cc3a65b685e25bd5f1a9c15637c766f5200207548e2a032e006c74'
$checksumtype = 'sha256'

$packageArgs = @{
    packageName   = $packageName
    fileType      = $installerType
    url           = $url
    silentArgs    = $silentArgs
    validExitCodes= $validExitCodes
    softwareName  = "$packageName*"
    checksum      = $checksum
    checksumType  = $checksumtype
  }

Install-ChocolateyPackage @packageArgs
