[![](https://img.shields.io/chocolatey/v/wamp-server?color=green&label=wamp-server)](https://chocolatey.org/packages/wamp-server) [![](https://img.shields.io/chocolatey/dt/wamp-server)](https://chocolatey.org/packages/wamp-server)

WampServer is a Windows web development environment. It allows you to create web applications 
with Apache2, PHP and a MySQL database. Alongside, PhpMyAdmin allows you to manage easily 
your databases.