[![](https://img.shields.io/chocolatey/v/argyll?color=green&label=argyll)](https://chocolatey.org/packages/argyll) [![](https://img.shields.io/chocolatey/dt/argyll)](https://chocolatey.org/packages/argyll)

## Argyll
Argyll Color Management System is an ICC compatible color management system, available as Open Source.
It supports accurate ICC profile creation for scanners, cameras and film recorders, and calibration and
profiling of displays and RGB &amp; CMYK printers. Device Link can be created with a wide variety of
advanced options, including specialized Video calibration standards  and 3dLuts. Spectral sample data is
supported, allowing a selection of illuminants observer types, and paper fluorescent whitener additive
compensation. Profiles can also incorporate source specific gamut mappings for perceptual and saturation
intents. Gamut mapping and profile linking uses the CIECAM02 appearance model, a unique gamut mapping
algorithm, and a wide selection of rendering intents. It also includes code for the fastest portable 8
bit raster color conversion engine available anywhere, as well as support for fast, fully accurate 16
bit conversion. Device color gamuts can also be viewed and compared using a VRML viewer. Comprehensive
documentation is provided for each major tool, and a general guide to using the tools for typical color
management tasks is also available. A mailing list provides support for more advanced usage.

Argyll is a collection of source code that compiles into a set of command line tools, licensed under the
GNU licensing terms.

Argyll also includes a general purpose ICC V2 profile format access library, icclib, and a general
purpose CGATS file format I/O library.

### Package-specific issue

If this package isn't up-to-date for some days, [Create an issue](https://github.com/tunisiano187/Chocolatey-packages/issues/new/choose)

[![Patreon](https://cdn.jsdelivr.net/gh/tunisiano187/Chocolatey-packages@d15c4e19c709e7148588d4523ffc6dd3cd3c7e5e/icons/patreon.png)](https://www.patreon.com/bePatron?u=39585820)
