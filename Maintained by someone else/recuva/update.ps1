import-module au

$releases = "https://www.ccleaner.com/recuva/download/standard"

function global:au_SearchReplace {
    @{
        'tools\chocolateyInstall.ps1' = @{
            "(^[$]url\s*=\s*)('.*')"      = "`$1'$($Latest.URL32)'"
            "(^[$]checksum\s*=\s*)('.*')" = "`$1'$($Latest.Checksum32)'"
        }
     }
}

function global:au_GetLatest {
	$url32 = $(((Invoke-WebRequest -Uri $releases -UseBasicParsing).Links | Where-Object {$_ -match 'exe'}).href).trim();
    
    $history = $(Invoke-WebRequest -Uri "https://www.ccleaner.com/recuva/version-history" );
    $version = $($history.AllElements | Where-Object {$_.TagName -eq "h6"})[0].innerHTML.split('v').split(' ')[1]
	
	$Latest = @{ URL32 = $url32; Version = $version }
    return $Latest
}

update -ChecksumFor 32
Test-Package
